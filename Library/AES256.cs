﻿using System;
using System.IO;

using System.Linq;
using System.Text;

using System.Security.Cryptography;

namespace Crypto {
    public class AES256 {
        public static byte[] IV;

        public static void GenerateKey(out string secretKey) {
            using (Aes aes = Aes.Create()) {
                aes.GenerateKey();
                secretKey = Convert.ToBase64String(aes.Key);
            }
        }

        public static string Encryption(string strData, string secretKey) {
            strData = Convert.ToBase64String(Encoding.UTF8.GetBytes(strData));
            using (Aes aes = Aes.Create()) {
                aes.GenerateIV();
                aes.Key = Convert.FromBase64String(secretKey);
                IV = aes.IV;

                byte[] encrypted;
                using (MemoryStream ms = new MemoryStream()) {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write)) {
                        cs.Write(Encoding.UTF8.GetBytes(strData), 0, strData.Length);
                        cs.Close();
                    }
                    encrypted = ms.ToArray();
                }

                return Convert.ToBase64String(encrypted.Concat(aes.IV).ToArray());
            }
        }

        public static string Decryption(string strData, string secretKey) {
            byte[] temp = Convert.FromBase64String(strData);

            byte[] salt = new byte[16];
            byte[] data = new byte[temp.Length - 16];

            Buffer.BlockCopy(temp, 0, data, 0, data.Length);
            Buffer.BlockCopy(temp, data.Length, salt, 0, salt.Length);

            using (Aes aes = Aes.Create()) {
                aes.IV = salt;
                aes.Key = Convert.FromBase64String(secretKey);

                string decrypted = "";
                using (MemoryStream ms = new MemoryStream()) {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write)) {
                        cs.Write(data, 0, data.Length);
                        cs.Close();
                    }
                    decrypted = Encoding.UTF8.GetString(ms.ToArray());
                }

                decrypted = Encoding.UTF8.GetString(Convert.FromBase64String(decrypted));

                return decrypted;
            }
        }
    }
}
