﻿using System;

using System.Text;

using System.Security.Cryptography;

namespace Crypto {
    public class RSA {
        public static void GenerateKeys(out string publicKey, out string privateKey) {
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(1024)) {
                try {
                    publicKey = rsa.ToXmlString(false);
                    privateKey = rsa.ToXmlString(true);
                } finally {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        public static string Encryption(string strData, string publicKey) {
            using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(1024)) {
                try {
                    rsa.FromXmlString(publicKey.ToString());

                    byte[] encrypted = rsa.Encrypt(Encoding.UTF8.GetBytes(strData), false);

                    return Convert.ToBase64String(encrypted);
                } finally {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        public static string Decryption(string strData, string privateKey) {
            using (var rsa = new RSACryptoServiceProvider(1024)) {
                try {
                    rsa.FromXmlString(privateKey);

                    var decrypted = rsa.Decrypt(Convert.FromBase64String(strData), false);

                    return Encoding.UTF8.GetString(decrypted);
                } finally {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }
    }
}
