﻿using System;

namespace Objects {
    public class Request {
        public string type { get; set; }
        public string code { get; set; }
        public string data { get; set; }
    }

    public class Response {
        public string code { get; set; }
        public string data { get; set; }
    }

    public class Authorization {
        public string login { get; set; }
        public string password { get; set; }
    }

    public class AuthRespond {
        public string error { get; set; }
        public string token { get; set; }
    }
}
