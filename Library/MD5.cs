﻿using System;
using System.Text;

namespace Crypto {
    public class MD5 {
        public static string Encryption(string data) {
            byte[] result = System.Security.Cryptography.MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(data));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < result.Length; i++) {
                sBuilder.Append(result[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}
