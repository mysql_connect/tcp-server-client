﻿using System;
using System.Threading;

using Newtonsoft.Json;

using Objects;

namespace Client {
    class Client {
        static void Main(string[] args) {

            TCPConnection connection = new TCPConnection("127.0.0.1", 2000);

            connection.ConnectedEvent += () => { Console.WriteLine("ConnectedEvent"); };
            connection.ConnectionEvent += () => { Console.WriteLine("ConnectionEvent"); };

            try {
                connection.StartAsync();

                AuthRespond respond = connection.Request<AuthRespond>("auth", new Authorization { login = "admin", password = "0000" });

                if (respond.error == "none") {
                    Console.WriteLine(respond.token);
                } else {
                    Console.WriteLine(respond.error);
                }

                Console.ReadKey();
                connection.Stop();

            } catch (Exception ex) {
                Console.WriteLine("Error: " + ex.Message);
            }

            Console.ReadKey();
        }
    }
}
