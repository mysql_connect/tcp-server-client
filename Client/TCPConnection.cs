﻿using System;

using System.IO;
using System.Net.Sockets;

using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Objects;

namespace Client {
    class TCPConnection {
        public TCPConnection(string ip, int port) {
            this.address = ip;
            this.port = port;

            Crypto.RSA.GenerateKeys(out publicKey, out privateKey);
        }

        public TCPConnection(string address) : this(address, 2000) { }

        public async Task StartAsync() {
            await Task.Run(() => Start());
        }

        public void Start() {
            try {
                client = new TcpClient(address, port);
                stream = client.GetStream();

                writer = new BinaryWriter(stream);
                reader = new BinaryReader(stream);

                writer.Write(publicKey); writer.Flush();
                serverKey = reader.ReadString();

                while (true) {
                    Thread.Sleep(1000);
                    if (!IsConnected) {
                        throw new Exception("Connection broken.");
                    } else {
                        OnConnectedEvent();
                    }
                }
            } catch (Exception) {
                OnConnectionEvent();
            } finally {
                if (writer != null) writer.Close();
                if (reader != null) reader.Close();
                if (stream != null) stream.Close();
                if (client != null) client.Close();

                if (reconnection) Start();
            }
        }

        public T Request<T>(string type, object data) {
            string secret;
            try {
                if (IsConnected) {
                    Crypto.AES256.GenerateKey(out secret);

                    writer.Write(
                        JsonConvert.SerializeObject(
                            new Request {
                                type = type,
                                code = Crypto.RSA.Encryption(secret, serverKey),
                                data = Crypto.AES256.Encryption(JsonConvert.SerializeObject(data), secret)
                            }
                        )
                    );
                    writer.Flush();
                    Response responseObject = JsonConvert.DeserializeObject<Response>(reader.ReadString());

                    return JsonConvert.DeserializeObject<T>(
                        Crypto.AES256.Decryption(responseObject.data, Crypto.RSA.Decryption(responseObject.code, privateKey))
                    );
                } else {
                    Thread.Sleep(1000);
                    return Request<T>(type, data);
                }
            } catch (Exception) {
                throw new Exception("Request failed");
            }
        }

        public void Stop() {
            reconnection = false;

            if (writer != null) writer.Close();
            if (reader != null) reader.Close();
            if (stream != null) stream.Close();
            if (client != null) client.Close();
        }

        #region НЕ ЛЕЗЬ СЮДА... ТУТ КОСТЫЛЬ!!!
        public bool IsConnected {
            get {
                bool connected = true;
                try {
                    if (client.Client.Poll(0, SelectMode.SelectRead)) {
                        byte[] buff = new byte[1];
                        if (client.Client.Receive(buff, SocketFlags.Peek) == 0) {
                            connected = false;
                        }
                    }
                } catch (Exception) {
                    connected = false;
                }
                return connected;
            }
        }
        #endregion

        #region Events
        public delegate void ConnectedHandler();
        public delegate void ConnectionHandler();

        public event ConnectedHandler ConnectedEvent;
        public event ConnectionHandler ConnectionEvent;

        private void OnConnectedEvent() {
            ConnectedHandler handler = ConnectedEvent;

            if (handler == null) return;

            Delegate[] invocationList = handler.GetInvocationList();

            Parallel.ForEach<Delegate>(invocationList, (hndler) => {
                ((ConnectedHandler)hndler)();
            });
        }

        private void OnConnectionEvent() {
            ConnectionHandler handler = ConnectionEvent;

            if (handler == null) return;

            Delegate[] invocationList = handler.GetInvocationList();

            Parallel.ForEach<Delegate>(invocationList, (hndler) => {
                ((ConnectionHandler)hndler)();
            });
        }
        #endregion   

        public string publicKey, privateKey, serverKey; // RSA keys

        private bool reconnection = true;

        private NetworkStream stream;
        private BinaryWriter writer;
        private BinaryReader reader;

        private TcpClient client;

        private string address;
        private int port;
    }
}
