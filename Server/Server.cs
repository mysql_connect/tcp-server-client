﻿using System;

using System.Threading;
using System.Threading.Tasks;

namespace Server {
    class Server {
        public static void Main(string[] args) {
            Console.Title = "TcpServer 2.0.0";
            ConsoleLog.WriteLine("Starting the server...");

            TCPServer server = new TCPServer();

            RequestController.Load();

            try {
                server.Start();

                while (server.isServer) {
                    if (ConsoleLog.ReadLine() == "/stop") {
                        server.Stop();
                    }
                }

                server.Stop();
            } catch (Exception ex) {
                ConsoleLog.WriteLine("Error: " + ex.Message, ConsoleLog.Type.Error);
            }
            Thread.Sleep(2500);
        }
    }
}
