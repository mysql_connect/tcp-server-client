﻿using System;

using System.Threading;

namespace Server {
    class ConsoleLog {

        public static string readData = "";

        public enum Type {
            Info = ConsoleColor.DarkGreen,
            Warning = ConsoleColor.DarkYellow,
            Error = ConsoleColor.DarkRed
        };

        private static Mutex mutex = new Mutex();

        public static void WriteLine(string message, Type type = Type.Info) {
            mutex.WaitOne();

            if (Console.CursorTop > 0) {
                Console.SetCursorPosition(0, Console.CursorTop);
                Console.Write(new string(' ', Console.WindowWidth));
                Console.SetCursorPosition(0, Console.CursorTop - 1);
            }

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write($"{DateTime.Now.ToString()} ");

            Console.ForegroundColor = (ConsoleColor)type;
            Console.Write($"{type.ToString()}: ");

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(message);

            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.Write($"> {readData}");

            mutex.ReleaseMutex();
        }

        public static string ReadLine() {
            int curIndex = 0;
            while (true) {
                ConsoleKeyInfo readKey = Console.ReadKey(true);
                if (readKey.Key == ConsoleKey.Escape) {
                    Console.WriteLine();
                    return null;
                }

                if (readKey.Key == ConsoleKey.Enter) {
                    string result = readData;
                    Console.WriteLine();
                    readData = "";
                    return result;
                }

                if (readKey.Key == ConsoleKey.Backspace) {
                    if (curIndex > 0) {
                        readData = readData.Remove(readData.Length - 1);
                        Console.Write(readKey.KeyChar);
                        Console.Write(' ');
                        Console.Write(readKey.KeyChar);
                        curIndex--;
                    }
                } else if (!Char.IsControl(readKey.KeyChar)) {
                    readData += readKey.KeyChar;
                    Console.Write(readKey.KeyChar);
                    curIndex++;
                }
            }
        }

    }
}
