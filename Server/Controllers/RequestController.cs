﻿using System;

using Newtonsoft.Json;

using Objects;

namespace Server {
    class RequestController {
        public static void Load() {
            RequestHandler.On("auth", Auth);
        }

        public static object Auth(string data) {
            AuthRespond response = new AuthRespond() { error = "none"};
            try {
                Authorization auth = JsonConvert.DeserializeObject<Authorization>(data);
                if (auth.login == "admin" && auth.password == "0000") {
                    response.token = "success";
                } else {
                    response.error = "Login or password incorrect";
                }
            } catch (Exception) {
                response.error = "The authorization json is corrupted";
            }
            return response;
        }
    }
}
