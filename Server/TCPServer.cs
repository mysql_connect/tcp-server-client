﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Objects;

namespace Server {
    class TCPServer {
        public TCPServer(IPAddress address, int port) {
            this.address = address; this.port = port;

            Crypto.RSA.GenerateKeys(out publicKey, out privateKey);
            ConsoleLog.WriteLine("RSA keys have been generated. ");
        }

        public TCPServer(string adress, int port) : this(IPAddress.Parse(adress), port) { }
        public TCPServer(int port) : this(IPAddress.Any, port) { }
        public TCPServer() : this(2000) { }

        public void Start() {
            try {
                token = cts.Token;

                listener = new TcpListener(address, port);
                ConsoleLog.WriteLine($"Start listening...");

                listener.Start();
                ConsoleLog.WriteLine($"Start listening successfully.");
                ConsoleLog.WriteLine($"Waiting for clients connection");

                listener.BeginAcceptTcpClient(new AsyncCallback(AcceptClient), listener);
            } catch (Exception ex) {
                ConsoleLog.WriteLine("Error: " + ex.Message, ConsoleLog.Type.Error);
            }
        }

        private void AcceptClient(IAsyncResult ar) {
            if (token.IsCancellationRequested) return;
            string clientAddress = null;
            try {
                TcpListener listener = (TcpListener)ar.AsyncState;

                listener.BeginAcceptTcpClient(new AsyncCallback(AcceptClient), listener);

                using (TcpClient client = listener.EndAcceptTcpClient(ar)) {
                    clientAddress = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();
                    ConsoleLog.WriteLine($"{clientAddress} client conected...");

                    using (NetworkStream ns = client.GetStream()) {
                        using (token.Register(() => ns.Close())) {
                            using (BinaryReader reader = new BinaryReader(ns))
                            using (BinaryWriter writer = new BinaryWriter(ns)) {

                                string clientKey = reader.ReadString();
                                writer.Write(publicKey); writer.Flush();
                                ConsoleLog.WriteLine($"public key exchange");

                                Request request; string secret, response;
                                while (!token.IsCancellationRequested) {
                                    Crypto.AES256.GenerateKey(out secret);

                                    request = JsonConvert.DeserializeObject<Objects.Request>(reader.ReadString());
                                    request.data = Crypto.AES256.Decryption(request.data, Crypto.RSA.Decryption(request.code, privateKey));

                                    response = JsonConvert.SerializeObject(RequestHandler.Emit(request.type, request.data));

                                    ConsoleLog.WriteLine(
                                        String.Format(
                                            "{4}: [{0}    Request type = {1},{0}    Request data = {2},{0}    Respond data = {3}{0}];",
                                            Environment.NewLine,
                                            request.type,
                                            request.data,
                                            response,
                                            clientAddress
                                        )    
                                    );

                                    writer.Write(
                                        JsonConvert.SerializeObject(
                                            new Objects.Response {
                                                code = Crypto.RSA.Encryption(secret, clientKey),
                                                data = Crypto.AES256.Encryption(response, secret)
                                            }
                                        )
                                    );
                                    writer.Flush();
                                }

                            }
                        }
                    }
                }
            } catch (Exception) {
                ConsoleLog.WriteLine($"{clientAddress} client disconnected...");
            }
        }

        public void Stop() {
            if (token.IsCancellationRequested) return;

            cts.Cancel();

            Thread.Sleep(100);
            listener.Stop();

            ConsoleLog.WriteLine($"Listening stopped.");
        }

        public string publicKey, privateKey; // RSA keys

        public bool isServer { get { return token.IsCancellationRequested ? false : true; } }

        private CancellationTokenSource cts = new CancellationTokenSource();
        private CancellationToken token;
        private TcpListener listener;

        private IPAddress address;
        private int port;
    }
}
