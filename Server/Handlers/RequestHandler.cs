﻿using System;

using System.Collections.Generic;

namespace Server {
    class RequestHandler {
        private static Dictionary<string, Func<string, object>> callbacks = new Dictionary<string, Func<string, object>>();

        public static void On(string type, Func<string, object> callback) {
            callbacks[type] = callback;
        }

        public static object Emit(string type, string data) {
            if (callbacks.ContainsKey(type)) {
                return callbacks[type](data);
            } else {
                return "Error not found request";
            }
        }
    }
}
